﻿using Bmes.ViewModels.Catalogue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bmes.Services
{
    public interface ICatalogueService
    {
        PagedProductViewModel FetchProducts(string categorySlug, string brandSlug);
    }

}
