using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bmes.Database;
using Bmes.Repositories;
using Bmes.Repositories.Implementations;
using Bmes.Services;
using Bmes.Services.Implementations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Bmes
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BmesDbContext>(options => options.UseSqlite(Configuration["Data:BmesWebApp:ConnectionString"]));
            services.AddMemoryCache();
            services.AddSession();
            services.AddControllersWithViews();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IBrandRepository, BrandRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<ICartRepository, CartRepository>();
            services.AddTransient<ICartItemRepository, CartItemRepository>();
            services.AddTransient<IAddressRepository, AddressRepository>();
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderItemRepository, OrderItemRepository>();

            services.AddTransient<ICatalogueService, CatalogueService>();
            services.AddTransient<ICartService, CartService>();
            services.AddTransient<ICheckoutService, CheckoutService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //commit
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {

                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: null,
                    pattern: "catalogue/{category_slug}/{brand_slug}/page{page:int}",
                    defaults: new { controller = "Catalogue", action = "Index" }
                    );

                endpoints.MapControllerRoute(
                    name: null,
                    pattern: "page{page:int}",
                    defaults: new
                    {
                        controller = "Catalogue",
                        action = "Index"
                    }
                    );

                endpoints.MapControllerRoute(
                    name: null,
                    pattern: "catalogue/{category_slug}/{brand_slug}",
                    defaults: new
                    {
                        controller = "Catalogue",
                        action = "Index"
                    }
                    );

                endpoints.MapControllerRoute(
                    name: default,
                    pattern: "{controller=Catalogue}/{action=Index}/{id?}"

                    );

                

            }
            );
        }
    }
}
