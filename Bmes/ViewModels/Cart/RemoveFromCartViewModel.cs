﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bmes.ViewModels.Cart
{
    public class RemoveFromCartViewModel
    {
        public long CartItemId { get; set; }
    }
}
